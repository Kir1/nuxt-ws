import NotificationsWatcher from '@/components/layouts/NotificationsWatcher';

export default {
  components: {
    NotificationsWatcher
  },
};
