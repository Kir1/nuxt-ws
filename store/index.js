import {
  STORE_USER_ACTION_GET,
  STORE_USER_GET,
  STORE_USER_SET,
  STORE_REQUESTS_NOTIFICATIONS_ADD,
  STORE_REQUESTS_NOTIFICATIONS_REMOVE,
  STORE_REQUESTS_NOTIFICATIONS_GET, STORE_AUTHORIZATION, STORE_LOGOUT,
} from "~/constants/store";

export const state = () => ({
  user: null,
  requestsNotifications: [],
})

export const mutations = {
  [STORE_USER_SET](state, user) {
    state.user = user;
  },
  [STORE_REQUESTS_NOTIFICATIONS_ADD](state, message) {
    state.requestsNotifications.push(message);
  },
  [STORE_REQUESTS_NOTIFICATIONS_REMOVE](state, index) {
    state.requestsNotifications.splice(index, 1);
  },
}

export const getters = {
  [STORE_USER_GET](state) {
    return state.user;
  },
  [STORE_REQUESTS_NOTIFICATIONS_GET](state) {
    return state.requestsNotifications;
  },
}

export const actions = {
  async nuxtServerInit({dispatch}) {
    if (this.$cookies.get('token')) {
      await dispatch(STORE_USER_ACTION_GET);
    }
  },
  async [STORE_USER_ACTION_GET]({commit}) {
    const {$api, $notification} = this;
    try {
      const user = await $api.post(`user`).then(response => response.data.user);
      commit(STORE_USER_SET, user);
    } catch (err) {
      $notification.error('Не удалось загрузить ваши данные. Попробуйте зайти или авторизоваться позднее.');
    }
  },
  [STORE_AUTHORIZATION]({dispatch}, user) {
    return this.$api.post(`auth`, user).then(r => {
      this.$cookies.set('token', r.data.token);
      dispatch(STORE_USER_ACTION_GET);
      return r;
    });
  },
  [STORE_LOGOUT]({dispatch, commit}) {
    return this.$api.post(`logout`).then(() => {
      this.$cookies.remove('token');
      commit(STORE_USER_SET, null);
    });
  },
}


