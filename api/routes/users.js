const {Router} = require('express');

const router = Router();

const haveCorrectToken = (req) => {
  return isUser(req) || isUserAdmin(req);
};
const isUser = (req) => {
  return req.headers.authorization === 'Bearer 666';
};
const isUserAdmin = (req) => {
  return req.headers.authorization === 'Bearer 777';
};
const checkReq = (req, res, check, code, actionSuccess = null, actionError = null) => {
  if (check) {
    if (actionSuccess) {
      actionSuccess();
    } else {
      res.sendStatus(200);
    }
  } else {
    if (actionError) {
      actionError();
    } else {
      res.sendStatus(code);
    }
  }
};
const checkAuth = (req, res, actionSuccess = null, actionError = null) => {
  checkReq(req, res, haveCorrectToken(req), 401, actionSuccess, actionError);
};
const checkAnonim = (req, res, actionSuccess = null, actionError = null) => {
  checkReq(req, res, !haveCorrectToken(req), 403, actionSuccess, actionError);
};
const checkAdmin = (req, res, actionSuccess = null, actionError = null) => {
  checkReq(req, res, isUserAdmin(req), 403, actionSuccess, actionError);
};
const user = (name) => ({user: {name}});

router.post('/user', function (req, res, next) {
  checkAuth(req, res, () => {
    if (isUser(req)) {
      res.json(user('Иван'));
    } else if (isUserAdmin(req)) {
      res.json(user('Антон'));
    }
  });
})

router.post('/auth', function (req, res, next) {
  checkAnonim(req, res, () => {
    const {name} = req.body;
    if (name) {
      if (name === 'Иван') {
        res.json({token: '666'});
      } else if (name === 'Антон') {
        res.json({token: '777'});
      } else {
        res.status(404).json({errors: {name: 'Такого пользователя не существует0'}});
      }
    }
  });
})

router.post('/logout', function (req, res, next) {
  checkAuth(req, res);
})

module.exports = router;
