import {
  NOTIFICATIONS_TYPE_ERROR,
  NOTIFICATIONS_TYPE_INFO,
  NOTIFICATION_TYPE_SUCCESS
} from "~/plugins/notification/constants";
import {STORE_REQUESTS_NOTIFICATIONS_ADD} from "~/constants/store";

export default function ({store}, inject) {
  const create = (text, type) => {
    store.commit(STORE_REQUESTS_NOTIFICATIONS_ADD, {
      text,
      options: {
        type,
      }
    });
  };

  const success = (text) => {
    create(text, NOTIFICATION_TYPE_SUCCESS);
  };

  const error = (text) => {
    create(text, NOTIFICATIONS_TYPE_ERROR);
  };

  const info = (text) => {
    create(text, NOTIFICATIONS_TYPE_INFO);
  };

  inject('notification', {
    success,
    error,
    info
  });
}
