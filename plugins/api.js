export default function ({app, $axios, store, $cookies, $notification, redirect}, inject) {
  const api = $axios.create({
    headers: {
      common: {
        Accept: 'text/plain, */*'
      },
    }
  });

  api.setBaseURL(process.env.baseUrl + "/api");

  api.onRequest(settings => {
    const token = $cookies.get('token');
    settings.headers.common.Authorization = token ? `Bearer ${token}` : '';
    return settings;
  });

  api.onError(error => {
    const status = error?.response?.status;

    if (status === undefined) {
      $notification.error('Нет подключения к интернету');
      return;
    }

    let key = '';
    let requestUrl = '';

    if (status === 401) {
      key = 'Вы неавторизованы';
      requestUrl = '/';
    } else if (status === 403) {
      key = 'Нет доступа';
    } else if (status >= 500) {
      key = 'Ошибка сервера';
    } else if (status === 404) {
      requestUrl = '/404';
    }

    if (requestUrl) {
      redirect(requestUrl);
    }

    if (key) {
      app.$notification.error(key);
    }
  });

  inject('api', api);
}
